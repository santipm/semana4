<?php $mts_options = get_option('splash'); ?>
<?php get_header(); ?>
<div id="page">
	<div class="<?php mts_article_class(); ?>">
		<div id="content_box">
			<h1 class="postsby">
				<span><?php _e("Search Results for:", "mythemeshop"); ?></span> <?php the_search_query(); ?>
			</h1>
			<?php 
			$selected_layout = (!empty($mts_options['mts_sorting']) && !empty($_COOKIE['selected_layout']) && $_COOKIE['selected_layout'] == 'grid' ? 'grid' : 'list');
			if($mts_options['mts_sorting'] == '1') { 
				?>	
				<div class="viewstyle">
					<span class="viewtext"><?php _e('Show Posts in','mythemeshop'); ?></span>
					<div class="viewsbox">
						<div id="list" <?php echo ($selected_layout == 'list' ? 'class="active"' : ''); ?>><a><i class="fa fa-list"></i> <?php _e('List View','mythemeshop'); ?></a></div>
						<div id="grid" <?php echo ($selected_layout == 'grid' ? 'class="active"' : ''); ?>><a><i class="fa fa-th"></i> <?php _e('Grid View','mythemeshop'); ?></a></div>
					</div>
					<div style="clear:both;"></div>
				</div>
			<?php } ?>		
			<?php $j = 0; if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article class="latestPost excerpt  <?php echo (++$j % 3 == 0) ? 'last' : ''; if ($selected_layout == 'grid') echo ' grid'; ?>">
					<header>
						<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow" id="featured-thumbnail">
								<?php echo '<div class="featured-thumbnail">'; the_post_thumbnail('featured',array('title' => '')); echo '</div>'; ?>
							<?php if(get_post_meta($post->ID, 'mts_overall_score', true)): ?>
								<span class="rating"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta($post->ID, 'mts_overall_score', true); ?>.png"/></span>
							<?php elseif (function_exists('wp_review_show_total')) :
								wp_review_show_total(true, 'rating');
							endif; ?></a>
						<h2 class="title front-view-title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
						<?php if($mts_options['mts_home_headline_meta'] == '1') { ?>
							<div class="post-info">
								<span class="theauthor"><i class="fa fa-user"></i> <?php  the_author_posts_link(); ?></span>  
								<span class="thetime"><i class="fa fa-calendar"></i> <?php the_time( get_option( 'date_format' ) ); ?></span>  
								<span class="thecategory"><i class="fa fa-tags"></i> <?php the_category(', ') ?></span> 
							</div>
						<?php } ?>
					</header>
					<div class="front-view-content">
						<?php echo mts_excerpt(48);?>
					</div>
				</article><!--.post excerpt-->
			<?php endwhile; else: ?>
				<div class="no-results">
					<h2><?php _e('We apologize for any inconvenience, please hit back on your browser or use the search form below.', 'mythemeshop'); ?></h2>
					<?php get_search_form(); ?>
				</div><!--noResults-->
			<?php endif; ?>
			<!--Start Pagination-->
			<?php if ($mts_options['mts_pagenavigation'] == '1' ) { ?>
				<?php  $additional_loop = 0; mts_pagination($additional_loop['max_num_pages']); ?>           
			<?php } else { ?>
				<div class="pagination">
					<ul>
						<li class="nav-previous"><?php next_posts_link( '<i class="fa fa-angle-left"></i> '. __( 'Previous', 'mythemeshop' ) ); ?></li>
						<li class="nav-next"><?php previous_posts_link( __( 'Next', 'mythemeshop' ).' <i class="fa fa-angle-right"></i>' ); ?></li>
					</ul>
				</div>
			<?php } ?>
			<!--End Pagination-->
		</div>
	</div>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>