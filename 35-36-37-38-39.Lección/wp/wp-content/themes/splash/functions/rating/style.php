<style type='text/css'>
.mts_metabox { padding: 10px 10px 0px 10px; }
.mts_metabox_field { margin-bottom: 15px; width: 100%; overflow: hidden; }
.mts_metabox_field label { font-weight: bold; float: left; width: 15%; }
.mts_metabox_field .field { float: left; width: 75%; }
.mts_metabox_field input[type=text] { width: 100%; }
</style>