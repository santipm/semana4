<?php
/*-----------------------------------------------------------------------------------

	Plugin Name: MyThemeShop Recent Reviews
	Description: A widget for displaying recent reviews.
	Version: 2

-----------------------------------------------------------------------------------*/
add_action('widgets_init', 'mts_rp_load_widgets');

function mts_rp_load_widgets()
{
	register_widget('mts_rp_Widget');
}

function r_string_limit_words($string, $word_limit)
{
	$words = explode(' ', $string, ($word_limit + 1));
	
	if(count($words) > $word_limit) {
		array_pop($words);
	}
	
	return implode(' ', $words);
}

class mts_rp_Widget extends WP_Widget {
	
	function mts_rp_Widget()
	{
		$widget_ops = array('classname' => 'mts_rp', 'description' => __('Displays recent Review Posts with star rating.','mythemeshop'));

		$control_ops = array('id_base' => 'mts_rp-widget');

		$this->WP_Widget('mts_rp-widget', __('MyThemeShop: Recent Reviews','mythemeshop'), $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);
		
		$posts = $instance['posts'];
		$images = true;
		
		echo $before_widget;
		
		if(!empty($title)) {
			echo $before_title.$title.$after_title;
		}		
		?>
		<!-- BEGIN WIDGET -->
		<div class="rp-wrarper">
			<h3><?php _e('Recent Reviews', 'mythemeshop'); ?></a></h3>
			<ul class="recent-reviews">
				<?php
				$recent_reviews = new WP_Query('posts_per_page='.$posts.'&post_type=reviews');
				if($recent_reviews->have_posts()): ?>
					<?php $j = 0; while($recent_reviews->have_posts()): $recent_reviews->the_post(); ?>						
					<li <?php echo (++$j % 3 == 0) ? 'class="last"' : ''; ?>>
						<div class="left">
							<a href='<?php the_permalink(); ?>' title='<?php the_title(); ?>' rel="nofollow">
								<?php if(has_post_thumbnail()): ?>
									<?php the_post_thumbnail('widgetthumb',array('title' => '')); ?>
								<?php else: ?>
									<img src="<?php echo get_template_directory_uri(); ?>/images/smallthumb.png" alt="<?php the_title(); ?>" class="wp-post-image" />
								<?php endif; ?>
								<div class="post-number"><i class="fa fa-comments"></i> <?php comments_number(__('0','mythemeshop'),__('1','mythemeshop'),'%'); ?></div>
							</a>
							<div class="clear"></div>
						</div>
						<a href='<?php the_permalink(); ?>' title='<?php the_title(); ?>' class="plink"><?php the_title(); ?></a>
						<?php echo strip_shortcodes(mts_excerpt(10)); ?>
						<div><?php if(get_post_meta(get_the_ID(), 'mts_overall_score', true)): ?>
							<span class="rating3"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta(get_the_ID(), 'mts_overall_score', true); ?>.png"/></span>
						<?php else: ?>
							<?php if (function_exists('wp_review_show_total')) wp_review_show_total(); ?>
						<?php endif; ?></div>
					</li>
					<?php endwhile; ?>
				<?php endif; ?>
			<ul>
		</div>
		<!-- END WIDGET -->
		<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['posts'] = $new_instance['posts'];
		$instance['show_images'] = true;
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('posts' => 3);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('posts'); ?>"><?php _e('Number of posts','mythemeshop'); ?>:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('posts'); ?>" name="<?php echo $this->get_field_name('posts'); ?>" type="number" min="1" step="1" value="<?php echo $instance['posts']; ?>" />
		</p>
	<?php }
}
?>