<?php
/*-----------------------------------------------------------------------------------*/
/*	Do not remove these lines, sky will fall on your head.
/*-----------------------------------------------------------------------------------*/
define('MTS_THEME_NAME', 'splash');
define('MTS_THEME_VERSION', '2.1.6');
require_once( dirname( __FILE__ ) . '/theme-options.php' );
if ( ! isset( $content_width ) ) $content_width = 1060;
$mts_options = get_option( MTS_THEME_NAME );

/*-----------------------------------------------------------------------------------*/
/*	Load Translation Text Domain
/*-----------------------------------------------------------------------------------*/
load_theme_textdomain( 'mythemeshop', get_template_directory().'/lang' );
if ( function_exists('add_theme_support') ) add_theme_support('automatic-feed-links');

// Custom translations
if ( !empty( $mts_options['translate'] )) {
    $mts_translations = get_option( 'mts_translations_'.MTS_THEME_NAME );//$mts_options['translations'];
    function mts_custom_translate( $translated_text, $text, $domain ) {
        if ( $domain == 'mythemeshop' || $domain == 'nhp-opts' ) {
            global $mts_translations;
            if ( !empty( $mts_translations[$text] )) {
                $translated_text = $mts_translations[$text];
            }
        }
    	return $translated_text;
        
    }
    add_filter( 'gettext', 'mts_custom_translate', 20, 3 );
}

/*-----------------------------------------------------------------------------------*/
/*	Disable theme updates from WordPress.org theme repository
/*-----------------------------------------------------------------------------------*/
function mts_disable_theme_update( $r, $url ) {
    if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
		return $r; // Not a theme update request
	$themes = unserialize( $r['body']['themes'] );
	unset( $themes[ get_option( 'template' ) ] );
	unset( $themes[ get_option( 'stylesheet' ) ] );
	$r['body']['themes'] = serialize( $themes );
	return $r;
}
add_filter( 'http_request_args', 'mts_disable_theme_update', 5, 2 );
add_filter( 'auto_update_theme', '__return_false' );


/*-----------------------------------------------------------------------------------*/
/*	Post Thumbnail Support
/*-----------------------------------------------------------------------------------*/
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 200, 200, true );
	add_image_size( 'featured', 200, 200, true ); //featured
	add_image_size( 'widgetthumb', 80, 80, true ); //widget
	add_image_size( 'slider', 610, 250, true ); //slider
}

function mts_get_thumbnail_url( $size = 'full' ) {
    global $post;
    if (has_post_thumbnail( $post->ID ) ) {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $size );
        return $image[0];
    }
    
    // use first attached image
    $images = get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
    if (!empty($images)) {
        $image = reset($images);
        $image_data = wp_get_attachment_image_src( $image->ID, $size );
        return $image_data[0];
    }
        
    // use no preview fallback
    if ( file_exists( get_template_directory().'/images/nothumb-'.$size.'.png' ) )
        return get_template_directory_uri().'/images/nothumb-'.$size.'.png';
    else
        return '';
}

/*-----------------------------------------------------------------------------------*/
/*  Alternative post templates
/*-----------------------------------------------------------------------------------*/
function mts_get_post_template( $default = 'default' ) {
    global $post;
    $single_template = $default;
    $posttemplate = get_post_meta( $post->ID, '_mts_posttemplate', true );
    
    if ( empty( $posttemplate ) || ! is_string( $posttemplate ) )
        return $single_template;
    
    if ( file_exists( dirname( __FILE__ ) . '/singlepost-'.$posttemplate.'.php' ) ) {
        $single_template = dirname( __FILE__ ) . '/singlepost-'.$posttemplate.'.php';
    }
    
    return $single_template;
}
function mts_set_post_template( $single_template ) {
     return mts_get_post_template( $single_template );
}
add_filter( 'single_template', 'mts_set_post_template' );

/*-----------------------------------------------------------------------------------*/
/*	Use first attached image as post thumbnail (fallback)
/*-----------------------------------------------------------------------------------*/
add_filter( 'post_thumbnail_html', 'mts_post_image_html', 10, 5 );
function mts_post_image_html( $html, $post_id, $post_image_id, $size, $attr ) {
    if ( has_post_thumbnail() || get_post_type( $post_id ) != 'post' )
        return $html;
    
    // use first attached image
    $images = get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post_id );
    if (!empty($images)) {
        $image = reset($images);
        return wp_get_attachment_image( $image->ID, $size, false, $attr );
    }
        
    // use no preview fallback
    if ( file_exists( get_template_directory().'/images/nothumb-'.$size.'.png' ) )
        return '<img src="'.get_template_directory_uri().'/images/nothumb-'.$size.'.png" class="attachment-'.$size.' wp-post-image" alt="'.get_the_title().'">';
    else
        return '<img src="'.get_template_directory_uri().'/images/nothumb.png" class="attachment-'.$size.' wp-post-image" alt="'.get_the_title().'">';
    
}

/*-----------------------------------------------------------------------------------*/
/*	Custom Menu Support
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'menus' );
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
			'primary-menu' => 'Primary Menu',
			'secondary-menu' => 'Secondary Menu',
		)
	);
}

/*-----------------------------------------------------------------------------------*/
/*	Javascsript
/*-----------------------------------------------------------------------------------*/
function mts_add_scripts() {
	$mts_options = get_option('splash');

	wp_enqueue_script('jquery');

	wp_register_script('customscript', get_template_directory_uri() . '/js/customscript.js', array('jquery'), MTS_THEME_VERSION);
	wp_enqueue_script ('customscript');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Parallax pages and posts
    if (is_singular()) {
        if ( basename( mts_get_post_template() ) == 'singlepost-parallax.php' || basename( get_page_template() ) == 'page-parallax.php' ) {
            wp_register_script ( 'jquery-parallax', get_template_directory_uri() . '/js/parallax.js' );
            wp_enqueue_script ( 'jquery-parallax' );
        }
    }
	
	global $is_IE;
	if ($is_IE) {
        wp_register_script ('html5shim', "http://html5shim.googlecode.com/svn/trunk/html5.js");
        wp_enqueue_script ('html5shim');
	}
}
add_action('wp_enqueue_scripts','mts_add_scripts');

function mts_load_footer_scripts() {  
	$mts_options = get_option('splash');
	
	//Slider
	if($mts_options['mts_featured_slider'] == '1' && !is_singular()) {
		wp_register_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array('jquery'), MTS_THEME_VERSION);
		wp_enqueue_script ('flexslider');
	}	

	if(is_singular()) {
		wp_register_script('jqueryraty', get_template_directory_uri() . '/js/jquery.raty.js');
		wp_enqueue_script('jqueryraty');
	}

	wp_register_script('jquery-cookie', get_template_directory_uri() . '/js/jquery.cookie.js');
	wp_enqueue_script('jquery-cookie');
	
	//Lightbox
	if($mts_options['mts_lightbox'] == '1') {
		wp_register_script('prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', true);
		wp_enqueue_script('prettyPhoto');
	}
	
	//Sticky Nav
	if($mts_options['mts_sticky_nav'] == '1') {
		wp_register_script('StickyNav', get_template_directory_uri() . '/js/sticky.js', true);
		wp_enqueue_script('StickyNav');
	}
    
    // Ajax Load More and Search Results
    wp_register_script( 'mts_ajax', get_template_directory_uri() . '/js/ajax.js', true );
	if( ! empty( $mts_options['mts_pagenavigation'] ) && $mts_options['mts_pagenavigation'] >= 2 && !is_singular() ) {
		wp_enqueue_script( 'mts_ajax' );
        
        wp_register_script( 'historyjs', get_template_directory_uri() . '/js/history.js', true );
        wp_enqueue_script( 'historyjs' );
        
        // Add parameters for the JS
        global $wp_query;
        $max = $wp_query->max_num_pages;
        $paged = ( get_query_var( 'paged' ) > 1 ) ? get_query_var( 'paged' ) : 1;
        $autoload = ( $mts_options['mts_pagenavigation'] == 3 );
        wp_localize_script(
        	'mts_ajax',
        	'mts_ajax_loadposts',
        	array(
        		'startPage' => $paged,
        		'maxPages' => $max,
        		'nextLink' => next_posts( $max, false ),
                'autoLoad' => $autoload,
                'i18n_loadmore' => __( 'Load More Posts', 'mythemeshop' ),
                'i18n_loading' => __('Loading...', 'mythemeshop'),
                'i18n_nomore' => __( 'No more posts.', 'mythemeshop' )
        	 )
        );
	}
    if ( ! empty( $mts_options['mts_ajax_search'] ) ) {
        wp_enqueue_script( 'mts_ajax' );
        wp_localize_script(
        	'mts_ajax',
        	'mts_ajax_search',
        	array(
				'url' => admin_url( 'admin-ajax.php' ),
        		'ajax_search' => '1'
        	 )
        );
    }
}  
add_action('wp_footer', 'mts_load_footer_scripts');  
if( !empty( $mts_options['mts_ajax_search'] )) {
    add_action( 'wp_ajax_mts_search', 'ajax_mts_search' );
    add_action( 'wp_ajax_nopriv_mts_search', 'ajax_mts_search' );
}
/*-----------------------------------------------------------------------------------*/
/* Enqueue CSS
/*-----------------------------------------------------------------------------------*/
function mts_enqueue_css() {
	$mts_options = get_option('splash');

	//slider
	if($mts_options['mts_featured_slider'] == '1' && !is_singular()) {
		wp_register_style('flexslider', get_template_directory_uri() . '/css/flexslider.css', array(), MTS_THEME_VERSION, 'all');
		wp_enqueue_style('flexslider');
	}

	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		//WooCommerce
		wp_register_style('woocommerce', get_template_directory_uri() . '/css/woocommerce2.css', array(), MTS_THEME_VERSION, 'all');
		wp_enqueue_style('woocommerce');
	}
	
	//lightbox
	if($mts_options['mts_lightbox'] == '1') {
		wp_register_style('prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', array(), null, 'all');
		wp_enqueue_style('prettyPhoto');
	}
	
	//Font Awesome
	wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), null, 'all');
	wp_enqueue_style('fontawesome');
	
	wp_enqueue_style('stylesheet', get_stylesheet_directory_uri() . '/style.css', array(), MTS_THEME_VERSION, 'all');

	//Responsive
	if($mts_options['mts_responsive'] == '1') {
        wp_enqueue_style('responsive', get_template_directory_uri() . '/css/responsive.css', array(), MTS_THEME_VERSION, 'all');
	}
	
	if ($mts_options['mts_bg_pattern_upload'] != '') {
		$mts_bg = $mts_options['mts_bg_pattern_upload'];
	} else {
		if($mts_options['mts_bg_pattern'] != '') {
			$mts_bg = get_template_directory_uri().'/images/'.$mts_options['mts_bg_pattern'].'.png';
		} 
	}
	$mts_sclayout = '';
	$mts_shareit_left = '';
	$mts_shareit_right = '';
	$mts_author = '';
	$mts_header_section = '';
	
	if ( is_page() || is_single() ) {
        $mts_sidebar_location = get_post_meta( get_the_ID(), '_mts_sidebar_location', true );
    } else {
        $mts_sidebar_location = '';
    }
	if ( $mts_sidebar_location != 'right' && ( $mts_options['mts_layout'] == 'sclayout' || $mts_sidebar_location == 'left' )) {
		$mts_sclayout = '.article { float: right; } #content_box { padding-left: 4%; padding-right: 0; }
		.sidebar.c-4-12 { float: left; padding-right: 0; }';
		if($mts_options['mts_floating_social'] == '1') {
			$mts_shareit_right = '.shareit, .shareit.top { margin: 0 630px 0; border-left: 0; }';
		}
	}

	if ($mts_options['mts_header_section2'] == '0') {
		$mts_header_section = '.logo-wrap, .widget-header { display: none; }
		#navigation { border-top: 0; }
		#header { min-height: 47px; }';
	}
	if($mts_options['mts_floating_social'] == '1') {
		$mts_shareit_left = '.shareit, .shareit.top { top: 275px; left: auto; z-index: 0; margin: 0 0 0 -123px; width: 90px; position: fixed; overflow: hidden; padding: 5px; border:none; border-right: 0;}
		.share-item {margin: 2px;}';
	}
	if($mts_options['mts_author_comment'] == '1') {
		$mts_author = '.bypostauthor {padding: 3%!important; background: #FAFAFA; width: 94%!important;}
		.bypostauthor:after { content: "'.__('Author','mythemeshop').'"; position: absolute; right: -1px; top: -1px; padding: 1px 10px; background: #818181; color: #FFF; }';
	}
	$custom_css = "
		body {background-color:{$mts_options['mts_bg_color']}; }
		body {background-image: url({$mts_bg});}
		.pace .pace-progress, #mobile-menu-wrapper ul li a:hover { background: {$mts_options['mts_color_scheme']}; }
		#wpmm-megamenu .wpmm-subcategories a.wpmm-subcategory:hover, #wpmm-megamenu .wpmm-subcategories a.wpmm-current-subcategory, #wpmm-megamenu.wpmm-megamenu-container .wpmm-posts .wpmm-entry-title a:hover, #wpmm-megamenu.wpmm-megamenu-container .wpmm-posts .wpmm-thumbnail:hover + .wpmm-entry-title a { color: {$mts_options['mts_color_scheme']} !important; }
		.main-navigation #navigation li a:hover, .main-navigation #navigation .current-menu-item, .main-navigation #navigation .sfHover > a, .main-navigation #navigation ul .current-menu-item:before, .main-navigation #navigation ul li:hover > a, .main-navigation #navigation ul li:hover:before,.main-navigation .ajax-search-results li a:hover, .cnumber, .postauthor h5, .copyrights a, .textwidget a, #logo a, .pnavigation2 a, .sidebar.c-4-12 a, .copyrights a:hover, footer .widget li a:hover, .reply a, .title a:hover, .post-info a:hover, .readMore a:hover, a { color:{$mts_options['mts_color_scheme']}; }	
		.currenttext, .pagination a:hover, .single .pagination a:hover .currenttext, #load-posts a:hover, input#mtscontact_submit:hover, .sidebar .popular-posts li:hover, .popular-posts .thecomment, #commentform input#submit:hover, .contactform #submit:hover, .mts-subscribe input[type='submit']:hover, #move-to-top:hover, .secondary-navigation #navigation .current-menu-item, .secondary-navigation #navigation ul li:hover, .tagcloud a:hover, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .woocommerce .bypostauthor:after, #searchsubmit, .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce-page nav.woocommerce-pagination ul li span.current, .woocommerce #content nav.woocommerce-pagination ul li span.current, .woocommerce-page #content nav.woocommerce-pagination ul li span.current, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce-page nav.woocommerce-pagination ul li a:hover, .woocommerce #content nav.woocommerce-pagination ul li a:hover, .woocommerce-page #content nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce-page nav.woocommerce-pagination ul li a:focus, .woocommerce #content nav.woocommerce-pagination ul li a:focus, .woocommerce-page #content nav.woocommerce-pagination ul li a:focus, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .post-number { background-color:{$mts_options['mts_color_scheme']}; color: #fff!important; }
		.secondary-navigation .wpmm-megamenu-showing { background-color:{$mts_options['mts_color_scheme']}!important; }
		.flex-control-thumbs .flex-active{ border-top:3px solid {$mts_options['mts_color_scheme']};}
		.secondary-navigation #navigation ul li:hover, .secondary-navigation #navigation ul li:hover + li, #navigation .mts-cart li:hover + li { border-color: {$mts_options['mts_color_scheme']}; }
		{$mts_sclayout}
		{$mts_shareit_left}
		{$mts_shareit_right}
		{$mts_author}
		{$mts_header_section}
		{$mts_options['mts_custom_css']}
			";
	wp_add_inline_style( 'stylesheet', $custom_css );
}
add_action('wp_enqueue_scripts', 'mts_enqueue_css', 99);

/*-----------------------------------------------------------------------------------*/
/*	Enable Widgetized sidebar and Footer
/*-----------------------------------------------------------------------------------*/
if ( function_exists('register_sidebar') ){
	register_sidebar(array(
		'name' => 'Header Ad',
		'description'   => __( 'Appears in Header', 'mythemeshop' ),
		'id' => 'widget-header' ,
		'before_widget' => '<div class="widget-header">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'name'=>'Sidebar',
		'id' => 'sidebar',
		'description'   => __( 'Appears on homepage, posts and pages', 'mythemeshop' ),
		'before_widget' => '<li id="%1$s" class="widget widget-sidebar %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<div class="widget-wrap"><h3>',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => 'Left Footer',
		'description'   => __( 'Appears in left side of footer.', 'mythemeshop' ),
		'id' => 'footer-1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-wrap"><h3 class="widget-title">',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => 'Center Footer',
		'description'   => __( 'Appears in center of footer.', 'mythemeshop' ),
		'id' => 'footer-2',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-wrap"><h3 class="widget-title">',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => 'Right Footer',
		'description'   => __( 'Appears in right side of footer.', 'mythemeshop' ),
		'id' => 'footer-3',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-wrap"><h3 class="widget-title">',
		'after_title' => '</h3></div>',
	));

	// Custom sidebars
    if ( !empty( $mts_options['mts_custom_sidebars'] ) && is_array( $mts_options['mts_custom_sidebars'] )) {
        foreach( $mts_options['mts_custom_sidebars'] as $sidebar ) {
            if ( !empty( $sidebar['mts_custom_sidebar_id'] ) && !empty( $sidebar['mts_custom_sidebar_id'] ) && $sidebar['mts_custom_sidebar_id'] != 'sidebar-' ) {
                register_sidebar( array( 'name' => ''.$sidebar['mts_custom_sidebar_name'].'', 'id' => ''.sanitize_title( strtolower( $sidebar['mts_custom_sidebar_id'] )).'', 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>' ));
            }
        }
    }
}

function mts_custom_sidebar() {
    $mts_options = get_option( MTS_THEME_NAME );
    
	// Default sidebar
	$sidebar = 'Sidebar';

	if ( is_home() && !empty( $mts_options['mts_sidebar_for_home'] )) $sidebar = $mts_options['mts_sidebar_for_home'];	
    if ( is_single() && !empty( $mts_options['mts_sidebar_for_post'] )) $sidebar = $mts_options['mts_sidebar_for_post'];
    if ( is_page() && !empty( $mts_options['mts_sidebar_for_page'] )) $sidebar = $mts_options['mts_sidebar_for_page'];
    
    // Archives
	if ( is_archive() && !empty( $mts_options['mts_sidebar_for_archive'] )) $sidebar = $mts_options['mts_sidebar_for_archive'];
	if ( is_category() && !empty( $mts_options['mts_sidebar_for_category'] )) $sidebar = $mts_options['mts_sidebar_for_category'];
    if ( is_tag() && !empty( $mts_options['mts_sidebar_for_tag'] )) $sidebar = $mts_options['mts_sidebar_for_tag'];
    if ( is_date() && !empty( $mts_options['mts_sidebar_for_date'] )) $sidebar = $mts_options['mts_sidebar_for_date'];
	if ( is_author() && !empty( $mts_options['mts_sidebar_for_author'] )) $sidebar = $mts_options['mts_sidebar_for_author'];
    
    // Other
    if ( is_search() && !empty( $mts_options['mts_sidebar_for_search'] )) $sidebar = $mts_options['mts_sidebar_for_search'];
	if ( is_404() && !empty( $mts_options['mts_sidebar_for_notfound'] )) $sidebar = $mts_options['mts_sidebar_for_notfound'];
	
    // Woo
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
        if ( is_shop() || is_product_category() ) {
            if ( !empty( $mts_options['mts_sidebar_for_shop'] ))
                $sidebar = $mts_options['mts_sidebar_for_shop'];
            else
                $sidebar = 'shop-sidebar'; // default
        }
	    if ( is_product() ) {
            if ( !empty( $mts_options['mts_sidebar_for_product'] ))
                $sidebar = $mts_options['mts_sidebar_for_product'];
            else
                $sidebar = 'product-sidebar'; // default
	    }
    }
    
	// Page/post specific custom sidebar
	if ( is_page() || is_single() ) {
		wp_reset_postdata();
		global $post;
        $custom = get_post_meta( $post->ID, '_mts_custom_sidebar', true );
		if ( !empty( $custom )) $sidebar = $custom;
	}

	return $sidebar;
}
/*-----------------------------------------------------------------------------------*/
/*  Load Widgets & Shortcodes
/*-----------------------------------------------------------------------------------*/
if (!function_exists('mts_load_theme_widgets')) {
	function mts_load_theme_widgets() {
		// Add the 125x125 Ad Block Custom Widget
		include_once("functions/widget-ad125.php");

		// Add the 300x250 Ad Block Custom Widget
		include_once("functions/widget-ad300.php");

		// Add the 728x90 Ad Block Custom Widget
		include_once("functions/widget-ad728.php");

		// Add the Tabbed Custom Widget
		include_once("functions/widget-tabs.php");

		// Add the Latest Tweets Custom Widget
		include_once("functions/widget-tweets.php");

		// Add Recent Posts Widget
		include_once("functions/widget-recentposts.php");

		// Add Related Posts Widget
		include_once("functions/widget-relatedposts.php");

		// Add Popular Posts Widget
		include_once("functions/widget-popular.php");

		// Add RecentPosts Widget
		include_once("functions/widget-reviews.php");

		// Add Facebook Like box Widget
		include_once("functions/widget-fblikebox.php");

		// Add Subscribe Widget
		include_once("functions/widget-subscribe.php");

		// Add Social Profile Widget
		include_once("functions/widget-social.php");

		// Add Category Posts Widget
		include_once("functions/widget-catposts.php");
	}
}
mts_load_theme_widgets(); // child theme compatibility

// Add Welcome message
include_once("functions/welcome-message.php");

// TGM Plugin Activation
include_once( "functions/plugin-activation.php" );

// Theme Functions
include_once("functions/theme-actions.php");

// Add Rating
include_once('functions/rating/metaboxes.php');
include_once('functions/rating/rating.php');

// Nav menus custom fields
include_once('functions/nav-menu.php');

// AJAX Contact Form - mts_contact_form()
include_once( 'functions/contact-form.php' );

// Post/page editor meta boxes
include_once( "functions/metaboxes.php" );

/*-----------------------------------------------------------------------------------*/
/*	Filters customize wp_title
/*-----------------------------------------------------------------------------------*/
function mts_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'mythemeshop' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'mts_wp_title', 10, 2 );

/*-----------------------------------------------------------------------------------*/
/*	Filters that allow shortcodes in Text Widgets
/*-----------------------------------------------------------------------------------*/
add_filter('widget_text', 'shortcode_unautop');
add_filter('widget_text', 'do_shortcode');
add_filter('the_content_rss', 'do_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Custom Comments template
/*-----------------------------------------------------------------------------------*/
function mts_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<div id="comment-<?php comment_ID(); ?>" style="position:relative;">
			<div class="comment-author vcard">
				<?php echo get_avatar( $comment->comment_author_email, 75 ); ?>
				<?php printf(__('<span class="fn">%s</span>', 'mythemeshop'), get_comment_author_link()) ?> 
				<?php $mts_options = get_option('splash'); if($mts_options['mts_comment_date'] == '1') { ?>
					<span class="ago"><i class="fa fa-calendar"></i> <?php comment_date(get_option( 'date_format' )); ?></span>
				<?php } ?>
				<span class="comment-meta">
					<?php edit_comment_link(__('(Edit)', 'mythemeshop'),'  ','') ?>
				</span>
			</div>
			<?php if ($comment->comment_approved == '0') : ?>
				<em><?php _e('Your comment is awaiting moderation.', 'mythemeshop') ?></em>
				<br />
			<?php endif; ?>
			<div class="commentmetadata">
			<?php comment_text() ?>
            <div class="reply">
				<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			</div>
			</div>
		</div>
	</li>
<?php }


/*-----------------------------------------------------------------------------------*/
/*	Wrap videos in .responsive-video div
/*-----------------------------------------------------------------------------------*/
function mts_responsive_video( $data ) {
    return '<div class="flex-video">' . $data . '</div>';
}
add_filter( 'embed_oembed_html', 'mts_responsive_video' );


/*-----------------------------------------------------------------------------------*/
/*	excerpt
/*-----------------------------------------------------------------------------------*/
function mts_excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt);
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

/*-----------------------------------------------------------------------------------*/
/* nofollow to next/previous links
/*-----------------------------------------------------------------------------------*/
function mts_pagination_add_nofollow($content) {
    return 'rel="nofollow"';
}
add_filter('next_posts_link_attributes', 'mts_pagination_add_nofollow' );
add_filter('previous_posts_link_attributes', 'mts_pagination_add_nofollow' );

/*-----------------------------------------------------------------------------------*/
/* Nofollow to category links
/*-----------------------------------------------------------------------------------*/
add_filter( 'the_category', 'mts_add_nofollow_cat' ); 
function mts_add_nofollow_cat( $text ) {
$text = str_replace('rel="category tag"', 'rel="nofollow"', $text); return $text;
}

/*-----------------------------------------------------------------------------------*/	
/* nofollow post author link
/*-----------------------------------------------------------------------------------*/
add_filter('the_author_posts_link', 'mts_nofollow_the_author_posts_link');
function mts_nofollow_the_author_posts_link ($link) {
return str_replace('<a href=', '<a rel="nofollow" href=',$link); 
}

/*-----------------------------------------------------------------------------------*/	
/* nofollow to reply links
/*-----------------------------------------------------------------------------------*/
function mts_add_nofollow_to_reply_link( $link ) {
return str_replace( '")\'>', '")\' rel=\'nofollow\'>', $link );
}
add_filter( 'comment_reply_link', 'mts_add_nofollow_to_reply_link' );

/*-----------------------------------------------------------------------------------*/
/* removes the WordPress version from your header for security
/*-----------------------------------------------------------------------------------*/
function wb_remove_version() {
	return '<!--Theme by MyThemeShop.com-->';
}
add_filter('the_generator', 'wb_remove_version');
	
/*-----------------------------------------------------------------------------------*/
/* Removes Trackbacks from the comment count
/*-----------------------------------------------------------------------------------*/
add_filter('get_comments_number', 'mts_comment_count', 0);
function mts_comment_count( $count ) {
	if ( ! is_admin() ) {
		global $id;
		$comments = get_comments('status=approve&post_id=' . $id);
		$comments_by_type = separate_comments($comments);
		return count($comments_by_type['comment']);
	} else {
		return $count;
	}
}

/*-----------------------------------------------------------------------------------*/
/* adds a class to the post if there is a thumbnail
/*-----------------------------------------------------------------------------------*/
function has_thumb_class($classes) {
	global $post;
	if( has_post_thumbnail($post->ID) ) { $classes[] = 'has_thumb'; }
		return $classes;
}
add_filter('post_class', 'has_thumb_class');

/*-----------------------------------------------------------------------------------*/	
/* AJAX Search results
/*-----------------------------------------------------------------------------------*/
function ajax_mts_search() {
    $query = $_REQUEST['q']; // It goes through esc_sql() in WP_Query
    $search_query = new WP_Query( array( 's' => $query, 'posts_per_page' => 3 )); 
    $search_count = new WP_Query( array( 's' => $query, 'posts_per_page' => -1 ));
    $search_count = $search_count->post_count;
    if ( !empty( $query ) && $search_query->have_posts() ) : 
        //echo '<h5>Results for: '. $query.'</h5>';
        echo '<ul class="ajax-search-results">';
        while ( $search_query->have_posts() ) : $search_query->the_post();
            ?><li>
    			<a href="<?php the_permalink(); ?>">
				    <?php the_post_thumbnail( 'widgetthumb', array( 'title' => '' )); ?>
    				<?php the_title(); ?>	
    			</a>
    			<div class="meta">
    					<span class="thetime"><?php the_time( 'F j, Y' ); ?></span>
    			</div> <!-- / .meta -->

    		</li>	
    		<?php
        endwhile;
        echo '</ul>';
        echo '<div class="ajax-search-meta"><span class="results-count">'.$search_count.' '.__( 'Results', 'mythemeshop' ).'</span><a href="'.get_search_link( $query ).'" class="results-link">Show all results</a></div>';
    else:
        echo '<div class="no-results">'.__( 'No results found.', 'mythemeshop' ).'</div>';
    endif;
        
    exit; // required for AJAX in WP
}

/*-----------------------------------------------------------------------------------*/	
/* Breadcrumb
/*-----------------------------------------------------------------------------------*/
function mts_the_breadcrumb() {
	echo '<a href="';
	echo home_url();
	echo '" rel="nofollow"><i class="fa fa-home"></i>&nbsp;'.__('Home','mythemeshop');
	echo "</a>";
	if (is_category() || is_single()) {
		echo "&nbsp;/&nbsp;";
		the_category(' &bull; ');
			if (is_single()) {
				echo "&nbsp;/&nbsp;";
				the_title();
			}
	} elseif (is_page()) {
		echo "&nbsp;/&nbsp;";
		echo the_title();
	} elseif (is_search()) {
		echo "&nbsp;/&nbsp;".__('Search Results for','mythemeshop')."... ";
		echo '"<em>';
		echo the_search_query();
		echo '</em>"';
	}
}

/*------------[ pagination ]-------------*/
if (!function_exists('mts_pagination')) {
    function mts_pagination($pages = '', $range = 2) { 
        $showitems = ($range * 3)+1;
        global $paged; if(empty($paged)) $paged = 1;
        if($pages == '') {
            global $wp_query; $pages = $wp_query->max_num_pages; 
            if(!$pages){ $pages = 1; } 
        }
        if(1 != $pages) { 
            echo "<div class='pagination'><ul>";
            if($paged > 2 && $paged > $range+1 && $showitems < $pages) 
                echo "<li><a rel='nofollow' href='".get_pagenum_link(1)."'><i class='fa fa-angle-double-left'></i> ".__('First','mythemeshop')."</a></li>";
            if($paged > 1 && $showitems < $pages) 
                echo "<li><a rel='nofollow' href='".get_pagenum_link($paged - 1)."' class='inactive'><i class='fa fa-angle-left'></i> ".__('Previous','mythemeshop')."</a></li>";
            for ($i=1; $i <= $pages; $i++){ 
                if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) { 
                    echo ($paged == $i)? "<li class='current'><span class='currenttext'>".$i."</span></li>":"<li><a rel='nofollow' href='".get_pagenum_link($i)."' class='inactive'>".$i."</a></li>";
                } 
            } 
            if ($paged < $pages && $showitems < $pages) 
                echo "<li><a rel='nofollow' href='".get_pagenum_link($paged + 1)."' class='inactive'>".__('Next','mythemeshop')." <i class='fa fa-angle-right'></i></a></li>";
            if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) 
                echo "<li><a rel='nofollow' class='inactive' href='".get_pagenum_link($pages)."'>".__('Last','mythemeshop')." <i class='fa fa-angle-double-right'></i></a></li>";
                echo "</ul></div>"; 
        }
    }
}

/*-----------------------------------------------------------------------------------*/
/* Redirect feed to feedburner
/*-----------------------------------------------------------------------------------*/
$mts_options = get_option('splash');
if ( isset($mts_options['mts_feedburner']) != '') {
function mts_rss_feed_redirect() {
    $mts_options = get_option('splash');
    global $feed;
    $new_feed = $mts_options['mts_feedburner'];
    if (!is_feed()) {
            return;
    }
    if (preg_match('/feedburner/i', $_SERVER['HTTP_USER_AGENT'])){
            return;
    }
    if ($feed != 'comments-rss2') {
            if (function_exists('status_header')) status_header( 302 );
            header("Location:" . $new_feed);
            header("HTTP/1.1 302 Temporary Redirect");
            exit();
    }
}
add_action('template_redirect', 'mts_rss_feed_redirect');
}

/*-----------------------------------------------------------------------------------*/
/* Single Post Pagination
/*-----------------------------------------------------------------------------------*/
function mts_wp_link_pages_args_prevnext_add($args)
{
    global $page, $numpages, $more, $pagenow;
    if (!$args['next_or_number'] == 'next_and_number')
        return $args; 
    $args['next_or_number'] = 'number'; 
    if (!$more)
        return $args; 
    if($page-1) 
        $args['before'] .= _wp_link_page($page-1)
        . $args['link_before']. $args['previouspagelink'] . $args['link_after'] . '</a>'
    ;
    if ($page<$numpages) 
    
        $args['after'] = _wp_link_page($page+1)
        . $args['link_before'] . $args['nextpagelink'] . $args['link_after'] . '</a>'
        . $args['after']
    ;
    return $args;
}
add_filter('wp_link_pages_args', 'mts_wp_link_pages_args_prevnext_add');

/*-----------------------------------------------------------------------------------*/
/* WooCommerce
/*-----------------------------------------------------------------------------------*/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
add_theme_support('woocommerce');

// Register Shop and Single Product Sidebar
register_sidebar(array(
	'name' => 'Shop Page Sidebar',
	'description'   => __( 'Appears on Shop main page and product archive pages.', 'mythemeshop' ),
	'id' => 'shop-sidebar',
	'before_widget' => '<li id="%1$s" class="widget widget-sidebar %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<div class="widget-wrap"><h3>',
	'after_title' => '</h3></div>',
));
register_sidebar(array(
	'name' => 'Single Product Sidebar',
	'description'   => __( 'Appears on single product pages.', 'mythemeshop' ),
	'id' => 'product-sidebar',
	'before_widget' => '<li id="%1$s" class="widget widget-sidebar %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<div class="widget-wrap"><h3>',
	'after_title' => '</h3></div>',
));

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

// Redefine woocommerce_output_related_products()
function woocommerce_output_related_products() {
woocommerce_related_products(array('posts_per_page' => 3, 'columns' => 1));
}

/*** Hook in on activation */
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'mythemeshop_woocommerce_image_dimensions', 1 );
 
/*** Define image sizes */
function mythemeshop_woocommerce_image_dimensions() {
  	$catalog = array(
		'width' 	=> '228',	// px
		'height'	=> '228',	// px
		'crop'		=> 1 		// true
	);
	$single = array(
		'width' 	=> '293',	// px
		'height'	=> '293',	// px
		'crop'		=> 1 		// true
	);
	$thumbnail = array(
		'width' 	=> '80',	// px
		'height'	=> '80',	// px
		'crop'		=> 0 		// false
	); 
	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}

add_filter ( 'woocommerce_product_thumbnails_columns', 'mts_thumb_cols' );
 function mts_thumb_cols() {
     return 4; // .last class applied to every 4th thumbnail
 }
}

// Display 24 products per page. Goes in functions.php
$mts_home_producst = $mts_options['mts_shop_products'];
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return '.$mts_home_producst.';' ), 20 );

/*------------[ Cart ]-------------*/
if ( ! function_exists( 'mts_cart' ) ) {
	function mts_cart() { 
	global $mts_options;
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
?>
<div class="mts-cart">
	<?php global $woocommerce; ?>
	<ul>
	<li><i class="fa fa-user"></i>
		<?php if ( is_user_logged_in() ) { ?>
			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','mythemeshop'); ?>"><?php _e('My Account','mythemeshop'); ?></a>
		<?php } 
		else { ?>
			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','mythemeshop'); ?>"><?php _e('Login ','mythemeshop'); ?></a>
		<?php } ?>
	</li>
	<li>
		<i class="fa fa-shopping-cart"></i> <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'mythemeshop'); ?>"></a>
	</li>
	</ul>
</div>
<?php } }

// Ensure cart contents update when products are added to the cart via AJAX
add_filter('add_to_cart_fragments', 'mts_header_add_to_cart_fragment');
 
function mts_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	ob_start();	?>
	
	<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'mythemeshop'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'mythemeshop'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
	
	<?php $fragments['a.cart-contents'] = ob_get_clean();
	return $fragments;
}
}

/*-----------------------------------------------------------------------------------*/
/* add <!-- next-page --> button to tinymce
/*-----------------------------------------------------------------------------------*/
add_filter('mce_buttons','wysiwyg_editor');
function wysiwyg_editor($mce_buttons) {
   $pos = array_search('wp_more',$mce_buttons,true);
   if ($pos !== false) {
       $tmp_buttons = array_slice($mce_buttons, 0, $pos+1);
       $tmp_buttons[] = 'wp_page';
       $mce_buttons = array_merge($tmp_buttons, array_slice($mce_buttons, $pos+1));
   }
   return $mce_buttons;
}

/*-----------------------------------------------------------------------------------*/
/*	Custom Gravatar Support
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'mts_custom_gravatar' ) ) {
    function mts_custom_gravatar( $avatar_defaults ) {
        $mts_avatar = get_bloginfo('template_directory') . '/images/gravatar.png';
        $avatar_defaults[$mts_avatar] = 'Custom Gravatar (/images/gravatar.png)';
        return $avatar_defaults;
    }
    add_filter( 'avatar_defaults', 'mts_custom_gravatar' );
}

/*-----------------------------------------------------------------------------------*/
/*  CREATE AND SHOW COLUMN FOR FEATURED IMAGES IN BACKEND
/*-----------------------------------------------------------------------------------*/

//Get Featured image
function mts_get_featured_image($post_ID) {  
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);  
    if ($post_thumbnail_id) {  
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'widgetthumb');  
        return $post_thumbnail_img[0];  
    }  
} 
function mts_columns_head($defaults) {  
	if (get_post_type() != 'product')
    	$defaults['featured_image'] = 'Featured Image';
    return $defaults;  
}  
function mts_columns_content($column_name, $post_ID) {  
    if ($column_name == 'featured_image') {  
        $post_featured_image = mts_get_featured_image($post_ID);  
        if ($post_featured_image) {  
            echo '<img width="80" height="80" src="' . $post_featured_image . '" />';  
        }  
    }  
} 
add_filter('manage_posts_columns', 'mts_columns_head');  
add_action('manage_posts_custom_column', 'mts_columns_content', 10, 2);

/*-----------------------------------------------------------------------------------*/
/*	Register Review Post
/*-----------------------------------------------------------------------------------*/
$wp_review_installed = defined( 'MTS_WP_REVIEW_DB_TABLE' );
if (!post_type_exists('reviews')) {
	register_post_type('reviews', array(
		'label' => __('Reviews', 'mythemeshop'),
		'singular_label' => __('Review', 'mythemeshop'),
		'public' => true,
		'show_ui' => $wp_review_installed,
		'capability_type' => 'post',
		'rewrite' => array('slug'=>'reviews','with_front' => false),
		'query_var' => false,
		'search_items' => __('Search Reviews', 'mythemeshop'),
		'not_found' =>  __('Nothing found', 'mythemeshop'),
		'not_found_in_trash' => __('Nothing found in Trash', 'mythemeshop'),
		'supports' => array('title', 'editor', 'author', 'comments', 'revisions', 'thumbnail'),
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'taxonomies' => array('category', 'post_tag'),
		'menu_icon' => 'dashicons-star-filled'//get_stylesheet_directory_uri() . '/images/star.png'
	));
}
add_filter( 'pre_get_posts', 'my_get_posts' );

add_action('init', 'reviews_add_default_boxes');
 
function reviews_add_default_boxes() {
    register_taxonomy_for_object_type('category', 'reviews');
    register_taxonomy_for_object_type('post_tag', 'reviews');
}

function my_get_posts( $query ) {

	if ( is_home() && $query->is_main_query() )
		$query->set( 'post_type', array( 'post','reviews') );

	return $query;
}

/*--ADD CUSTOM POST TYPES TO AUTHOR ARCHIVE PAGES--*/

function mts_custom_post_author_archive( $query ) {
    if ( $query->is_author )
        $query->set( 'post_type', array( 'reviews', 'post' ) );
    remove_action( 'pre_get_posts', 'custom_post_author_archive' );
}
add_action( 'pre_get_posts', 'mts_custom_post_author_archive' );


function mts_custom_post_category_archive( $query ) {
    if ( $query->is_category )
        $query->set( 'post_type', array( 'reviews', 'post' ) );
    remove_action( 'pre_get_posts', 'custom_post_category_archive' );
}
add_action( 'pre_get_posts', 'mts_custom_post_category_archive' );

function mts_custom_post_tag_archive( $query ) {
    if ( $query->is_tag )
        $query->set( 'post_type', array( 'reviews', 'post' ) );
    remove_action( 'pre_get_posts', 'custom_post_tag_archive' );
}
add_action( 'pre_get_posts', 'mts_custom_post_tag_archive' );

// Reviews update
function mts_convert_reviews() {
	$recent_reviews = get_posts('posts_per_page=-1&post_type=reviews');
	foreach ($recent_reviews as $review_post) {
		// check for old review data
		$old_review_score = get_post_meta( $review_post->ID, 'mts_overall_score', true );
		$old_review_criteria = get_post_meta( $review_post->ID, 'mts_critera_1', true );
		if ($old_review_score || $old_review_criteria) {
			// copy total
			update_post_meta( $review_post->ID, 'wp_review_total', $old_review_score );

			// get criteria and scores
			$criteria = array();
			for ($i=1; $i <= 5; $i++) { 
				$old_criteria = get_post_meta( $review_post->ID, "mts_critera_{$i}", true );
				if ($old_criteria) {
					$criteria[] = array('wp_review_item_title' => $old_criteria, 'wp_review_item_star' => get_post_meta( $review_post->ID, "mts_critera_{$i}_score", true ));
				}
			}
			update_post_meta( $review_post->ID, 'wp_review_item', $criteria );

			// Review description
			update_post_meta( $review_post->ID, 'wp_review_desc', get_post_meta( $review_post->ID, 'mts_pdescription', true ) );

			// set review type to 'star'
			update_post_meta( $review_post->ID, 'wp_review_type', 'star' );

			// set review heading to 'Review'
			update_post_meta( $review_post->ID, 'wp_review_heading', __('Review', 'mythemeshop') );

			// enable user review and review through comment
			update_post_meta( $review_post->ID, 'wp_review_userReview', array('1') );
			update_post_meta( $review_post->ID, 'wp_review_through_comment', 1 );

			// remove total
			delete_post_meta( $review_post->ID, 'mts_overall_score' );

			// remove review description
			delete_post_meta( $review_post->ID, 'mts_pdescription' );

			// remove criteria & score
			for ($i=1; $i <= 5; $i++) { 
				delete_post_meta( $review_post->ID, "mts_critera_{$i}" );
				delete_post_meta( $review_post->ID, "mts_critera_{$i}_score" );
			}
		}
	}
	// convert user ratings
	global $wpdb;

	$old_ratings_table = $wpdb->prefix.'ts_ratings';
	$new_ratings_table = $wpdb->prefix . 'mts_wp_reviews';
	
	$copy = $wpdb->query("INSERT INTO $new_ratings_table (blog_id,post_id,user_id,user_ip,rate,date) SELECT 1,post_id,0,IP,rating,now() FROM $old_ratings_table");
	$delete = $wpdb->query("TRUNCATE TABLE $old_ratings_table");
}
function mts_old_reviews_exist() {
	$recent_reviews = get_posts('posts_per_page=-1&post_type=reviews');
	foreach ($recent_reviews as $review_post) {
		// check for old review data
		$old_review_score = get_post_meta( $review_post->ID, 'mts_overall_score', true );
		$old_review_criteria = get_post_meta( $review_post->ID, 'mts_critera_1', true );
		if ($old_review_score || $old_review_criteria) {
			return true;
		}
	}
	return false;
}
add_action( 'admin_init', 'mts_update_reviews' );
function mts_update_reviews() {
	$reviews_version = get_option( 'splash_reviews_version' );
	if (!empty($_GET['mts_convert_reviews'])) {
		mts_convert_reviews();
		update_option( 'splash_reviews_version', MTS_THEME_VERSION );
		add_action( 'admin_notices', 'mts_update_reviews_success' );
	} elseif (version_compare($reviews_version, MTS_THEME_VERSION, '<')) {
		if (mts_old_reviews_exist()) {
			$wp_review_installed = defined( 'MTS_WP_REVIEW_DB_TABLE' );
			if ($wp_review_installed)
				add_action( 'admin_notices', 'mts_update_reviews_notice' );
			else
				add_action( 'admin_notices', 'mts_update_reviews_install_plugin' );
		} else {
			update_option( 'splash_reviews_version', MTS_THEME_VERSION );
		}
	}
}
function mts_update_reviews_notice() {
    ?>
    <div class="update-nag error">
        <p><?php _e( 'Reviews feature in Splash now requires WP Review plugin to function. Please convert old reviews to be able to edit them.'); ?><br />
        <a href="<?php echo add_query_arg('mts_convert_reviews', '1'); ?>" id="convert-reviews-link"><?php _e( 'Click here to convert now'); ?></a></p>
        <p><em><?php _e( 'This will make permanent changes, it is recommended to create a backup of your database before starting.', 'mythemeshop' ); ?></em></p>
        <script type="text/javascript">jQuery(document).ready(function($) { $('#convert-reviews-link').click(function() { return confirm('<?php _e('Are you sure you want to convert existing reviews now?'); ?>'); }); });</script>
    </div>
    <?php
}
function mts_update_reviews_install_plugin() {
    ?>
    <div class="update-nag error">
        <p><?php printf(__( 'Reviews feature in Splash now requires WP Review plugin to function. <br /><a href="%s">Please install and activate WP Review or WP Review Pro plugin</a>, and convert old reviews to be able to edit them.'), add_query_arg( 'page', 'mts-install-plugins', admin_url( 'themes.php' ) )); ?><br />
    </div>
    <?php 
}
function mts_update_reviews_success() {
    ?>
    <div class="updated">
        <p><?php _e( 'Reviews successfully updated.', 'mythemeshop' ); ?></p>
    </div>
    <?php
}

$new_review_options = array(
  'colors' => array(
    'color' => '#FFC200',
    'fontcolor' => '#EDEDED',
    'bgcolor1' => '#282828',
    'bgcolor2' => '#3D3D3D',
    'bordercolor' => '#282828'
  )
);
if ( function_exists( 'wp_review_theme_defaults' )) wp_review_theme_defaults( $new_review_options );

add_filter( 'wp_review_get_data', 'mts_add_proscons', 10, 5 );
function mts_add_proscons($review, $post_id, $type, $total, $items) {
    $options = get_option('wp_review_options');
	$colors = array();
	$colors['custom_colors'] = get_post_meta( $post_id, 'wp_review_custom_colors', true );
	$colors['color'] = get_post_meta( $post_id, 'wp_review_color', true );
	$colors['type']  = get_post_meta( $post_id, 'wp_review_type', true );
	$colors['fontcolor'] = get_post_meta( $post_id, 'wp_review_fontcolor', true );
	$colors['bgcolor1']  = get_post_meta( $post_id, 'wp_review_bgcolor1', true );
	$colors['bgcolor2']  = get_post_meta( $post_id, 'wp_review_bgcolor2', true );
	$colors['bordercolor']  = get_post_meta( $post_id, 'wp_review_bordercolor', true );
	$colors['total'] = get_post_meta( $post_id, 'wp_review_total', true );
	$custom_width        = get_post_meta( $post_id, 'wp_review_custom_width', true );
	$width        = get_post_meta( $post_id, 'wp_review_width', true );
	$align        = get_post_meta( $post_id, 'wp_review_align', true );
    
    if (!$colors['custom_colors'] && !empty($options['colors']) && is_array($options['colors'])) {
		$colors = array_merge($colors, $options['colors']);
	}
    $colors = apply_filters('wp_review_colors', $colors, $post_id);
	if (!$custom_width) {
		if (!empty($options['width'])) {
			$width = $options['width'];
		} else {
			$width = 50;
		}
	}
    $colors = apply_filters('wp_review_colors', $colors, $post_id);
	// check for proscons
	$pros = get_post_meta( $post_id, 'mts_pros', true );
	$cons = get_post_meta( $post_id, 'mts_cons', true );
	if ($pros || $cons) {
		$review = '<div class="review-including-proscons">'.$review;
		$review .= '<div class="proscons">';
			if(get_post_meta($post_id, 'mts_pros', true)):
				$review .= '<div class="pros"><b>Pros:</b>&nbsp;<span>'.get_post_meta($post_id, 'mts_pros', true).'</span></div>';
			endif;
			if(get_post_meta($post_id, 'mts_cons', true)):
				$review .= '<div class="cons"><b>Cons:</b>&nbsp;<span>'.get_post_meta($post_id, 'mts_cons', true).'</span></div>';
			endif;
		$review .= '</div></div>';
	}
	// add extra CSS: apply one color for features only
	$css = '<style type="text/css">
	#review.wp-review-'.$post_id.' .review-list li { background: '.$colors['bgcolor2'].' }
	#review.wp-review-'.$post_id.' { background: '.$colors['bgcolor1'].' }';

	if (function_exists('wp_review_comment_fields')) {
		if ($width == 100)
			$css .= '.proscons { width: 100%; }';
		else
			$css .= '.proscons { width: '.(max(100 - $width-3, 0)).'%; }';
			if (empty($align) || $align == 'left')
				$css .= '#review.wp-review-'.$post_id.' { margin-right: 2%; }';
			else
				$css .= '#review.wp-review-'.$post_id.' { margin-left: 2%; }';
	} else {
		$css .= '#review.wp-review-'.$post_id.' { width: 50%; float: left; margin-right: 2%; }
		.review-wrapper blockquote:before { display: none; } .review-total-wrapper span.review-total-box { float: left; text-align: left; width: 40%; padding-left: 15px; padding-top: 15px; } .review-total-wrapper { float: left; width: 100%; } .review-total-star { margin-top: 14px; margin-right: 15px; } .review-desc { width: 100%; }';
	}
	$css .= '</style>';
	return $review.$css;
}

// Mega Menu support
function megamenu_parent_element( $selector ) {
    return '.main-navigation, .secondary-navigation';
}
add_filter( 'wpmm_container_selector', 'megamenu_parent_element' );

function mts_menu_item_color( $item_output, $item_color, $item, $depth, $args ) {
	if (!empty($item_color)) {
    	if ($args->theme_location == 'secondary-menu'){
            $item_output .= '<style type="text/css">#menu-item-'. $item->ID . ':hover, #menu-item-'. $item->ID . '.wpmm-megamenu-showing, #menu-item-'. $item->ID . '.current-menu-item { background: '.$item_color.' !important; border-color: '.$item_color.' !important; } 
#menu-item-'. $item->ID . ':hover + li { border-left-color: '.$item_color.' !important; }</style>';
    	} else {
    		$item_output .= '<style type="text/css">#menu-item-'. $item->ID . ' a { color: '.$item_color.'!important; }</style>';
    	}
    }
    return $item_output;
}
add_filter( 'wpmm_color_output', 'mts_menu_item_color', 10, 5 );

/*-----------------------------------------------------------------------------------*/
/*  Thumbnail Upscale
/*  Enables upscaling of thumbnails for small media attachments, 
/*  to make sure it fits into it's supposed location.
/*  Cannot be used in conjunction with Retina Support.
/*-----------------------------------------------------------------------------------*/
if ( empty( $mts_options['mts_retina'] ) ) {
    function mts_image_crop_dimensions( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ) {
        if( !$crop )
        	return null; // let the wordpress default function handle this
    
        $aspect_ratio = $orig_w / $orig_h;
        $size_ratio = max( $new_w / $orig_w, $new_h / $orig_h );
    
        $crop_w = round( $new_w / $size_ratio );
        $crop_h = round( $new_h / $size_ratio );
    
        $s_x = floor( ( $orig_w - $crop_w ) / 2 );
        $s_y = floor( ( $orig_h - $crop_h ) / 2 );
    
        return array( 0, 0, ( int ) $s_x, ( int ) $s_y, ( int ) $new_w, ( int ) $new_h, ( int ) $crop_w, ( int ) $crop_h );
    }
    add_filter( 'image_resize_dimensions', 'mts_image_crop_dimensions', 10, 6 );
}
?>