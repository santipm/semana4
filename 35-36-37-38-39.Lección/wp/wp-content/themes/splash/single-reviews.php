<?php $mts_options = get_option('splash'); ?>
<?php get_header(); ?>
<div id="page" class="single">
	<article class="<?php mts_article_class(); ?>">
		<div id="content_box" >
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class('g post'); ?>>
					<div class="single_post">
						<?php if ($mts_options['mts_breadcrumb'] == '1') { ?>
							<div class="breadcrumb"><?php mts_the_breadcrumb(); ?></div>
						<?php } ?>
						<header>
							<h1 class="title single-title"><?php the_title(); ?></h1>
							<?php if($mts_options['mts_headline_meta'] == '1') { ?>
								<div class="post-info">
									<span class="theauthor"><i class="fa fa-user"></i> <?php  the_author_posts_link(); ?></span>  
									<span class="thetime"><i class="fa fa-calendar"></i> <?php the_time( get_option( 'date_format' ) ); ?></span>  
									<span class="thecategory"><i class="fa fa-tags"></i> <?php the_category(', ') ?></span>  
									<span class="thecomment"><i class="fa fa-comments"></i> <a rel="nofollow" href="<?php comments_link(); ?>"><?php echo comments_number();?></a></span>
								</div>
							<?php } ?>
						</header><!--.headline_area-->

						<div class="post-content box mark-links">
							<?php if($mts_options['mts_social_buttons'] == '1' && $mts_options['mts_social_buttons_position'] == '0') { ?>
								<!-- Start Share Buttons -->
								<div class="shareit top">
									<?php if($mts_options['mts_twitter'] == '1') { ?>
										<!-- Twitter -->
										<span class="share-item twitterbtn">
											<a href="https://twitter.com/share" class="twitter-share-button" data-via="<?php echo $mts_options['mts_twitter_username']; ?>">Tweet</a>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_gplus'] == '1') { ?>
										<!-- GPlus -->
										<span class="share-item gplusbtn">
											<g:plusone size="medium"></g:plusone>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_facebook'] == '1') { ?>
										<!-- Facebook -->
										<span class="share-item facebookbtn">
											<div id="fb-root"></div>
											<div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_linkedin'] == '1') { ?>
										<!--Linkedin -->
										<span class="share-item linkedinbtn">
											<script type="IN/Share" data-url="<?php get_permalink(); ?>"></script>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_stumble'] == '1') { ?>
										<!-- Stumble -->
										<span class="share-item stumblebtn">
											<su:badge layout="1"></su:badge>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_pinterest'] == '1') { ?>
										<!-- Pinterest -->
										<span class="share-item pinbtn">
											<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>" class="pin-it-button" count-layout="horizontal">Pin It</a>
										</span>
									<?php } ?>
								</div>
								<!-- end Share Buttons -->
							<?php } ?>
							<?php if ($mts_options['mts_posttop_adcode'] != '') { ?>
								<?php $toptime = $mts_options['mts_posttop_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$toptime day")), get_the_time("Y-m-d") ) >= 0) { ?>
									<div class="topad">
										<?php echo $mts_options['mts_posttop_adcode']; ?>
									</div>
								<?php } ?>
							<?php } ?>
							<?php if ($mts_options['mts_posttop_adcode'] != '') { ?>
								<?php $toptime = $mts_options['mts_posttop_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$toptime day")), get_the_time("Y-m-d") ) >= 0) { ?>
									<div class="topad">
										<?php echo $mts_options['mts_posttop_adcode']; ?>
									</div>
								<?php } ?>
							<?php } ?>
							<?php 
							// old reviews box
							$old_review_score = get_post_meta( $post->ID, 'mts_overall_score', true );
							$old_review_criteria = get_post_meta( $post->ID, 'mts_critera_1', true );
							if ($old_review_score || $old_review_criteria) {
							?>
							<div class="reviewbox">
								<div class="pdetails">
									<?php if(get_post_meta($post->ID, 'mts_overall_score', true)): ?>
										<h3><div class="overall-score"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta($post->ID, 'mts_overall_score', true); ?>.png" alt="" /></div></h3>
									<?php endif; ?>
									<div class="reviewmeta">
										<?php if(get_post_meta($post->ID, 'mts_critera_1', true)): ?>
										<div class="clearfix"><?php echo get_post_meta($post->ID, 'mts_critera_1', true); ?><span class="score"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta($post->ID, 'mts_critera_1_score', true); ?>.png" alt="" /></span></div>
										<?php endif; ?>
										<?php if(get_post_meta($post->ID, 'mts_critera_2', true)): ?>
										<div class="clearfix"><?php echo get_post_meta($post->ID, 'mts_critera_2', true); ?> <span class="score"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta($post->ID, 'mts_critera_2_score', true); ?>.png" alt="" /></span></div>
										<?php endif; ?>
										<?php if(get_post_meta($post->ID, 'mts_critera_3', true)): ?>
										<div class="clearfix"><?php echo get_post_meta($post->ID, 'mts_critera_3', true); ?> <span class="score"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta($post->ID, 'mts_critera_3_score', true); ?>.png" alt="" /></span></div>
										<?php endif; ?>
										<?php if(get_post_meta($post->ID, 'mts_critera_4', true)): ?>
										<div class="clearfix"><?php echo get_post_meta($post->ID, 'mts_critera_4', true); ?> <span class="score"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta($post->ID, 'mts_critera_4_score', true); ?>.png" alt="" /></span></div>
										<?php endif; ?>
										<?php if(get_post_meta($post->ID, 'mts_critera_5', true)): ?>
										<div class="clearfix"><?php echo get_post_meta($post->ID, 'mts_critera_5', true); ?> <span class="score"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta($post->ID, 'mts_critera_5_score', true); ?>.png" alt="" /></span></div>
										<?php endif; ?>
										<div id="user-rating" class="clearfix">
											<span><?php _e('User rating', 'mythemeshop'); ?></span>
											<div class="user-rating"></div>
										</div>
									</div>
								</div>
								<div class="proscons">
									<?php if(get_post_meta($post->ID, 'mts_pros', true) || get_post_meta($post->ID, 'mts_cons', true)): ?>
										<?php if(get_post_meta($post->ID, 'mts_pros', true)): ?>
											<div class="pros"><b>Pros:</b>&nbsp;<span><?php echo get_post_meta($post->ID, 'mts_pros', true); ?></span></div>
										<?php endif; ?>
										<?php if(get_post_meta($post->ID, 'mts_cons', true)): ?>
											<div class="cons"><b>Cons:</b>&nbsp;<span><?php echo get_post_meta($post->ID, 'mts_cons', true); ?></span></div>
										<?php endif; ?>
									<?php else: ?>
										<?php if(get_post_meta($post->ID, 'mts_pdescription', true)): ?>
											<div class="cons"><span><?php echo get_post_meta($post->ID, 'mts_pdescription', true); ?></span></div>
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>
							<?php 
							} else {
							// pros/cons added with wp_review_get_data filter
							}
							?>
							<?php the_content(); ?>
							<?php wp_link_pages(array('before' => '<div class="pagination">', 'after' => '</div>', 'link_before'  => '<span class="current"><span class="currenttext">', 'link_after' => '</span></span>', 'next_or_number' => 'next_and_number', 'nextpagelink' => __('Next','mythemeshop'), 'previouspagelink' => __('Previous','mythemeshop'), 'pagelink' => '%','echo' => 1 )); ?>
							<?php if ($mts_options['mts_postend_adcode'] != '') { ?>
								<?php $endtime = $mts_options['mts_postend_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$endtime day")), get_the_time("Y-m-d") ) >= 0) { ?>
									<div class="bottomad">
										<?php echo $mts_options['mts_postend_adcode'];?>
									</div>
								<?php } ?>
							<?php } ?> 
							<?php if($mts_options['mts_social_buttons'] == '1' && $mts_options['mts_social_buttons_position'] == '1') { ?>
								<!-- Start Share Buttons -->
								<div class="shareit">
									<?php if($mts_options['mts_twitter'] == '1') { ?>
										<!-- Twitter -->
										<span class="share-item twitterbtn">
											<a href="https://twitter.com/share" class="twitter-share-button" data-via="<?php echo $mts_options['mts_twitter_username']; ?>">Tweet</a>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_gplus'] == '1') { ?>
										<!-- GPlus -->
										<span class="share-item gplusbtn">
											<g:plusone size="medium"></g:plusone>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_facebook'] == '1') { ?>
										<!-- Facebook -->
										<span class="share-item facebookbtn">
											<div id="fb-root"></div>
											<div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_linkedin'] == '1') { ?>
										<!--Linkedin -->
										<span class="share-item linkedinbtn">
											<script type="IN/Share" data-url="<?php get_permalink(); ?>"></script>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_stumble'] == '1') { ?>
										<!-- Stumble -->
										<span class="share-item stumblebtn">
											<su:badge layout="1"></su:badge>
										</span>
									<?php } ?>
									<?php if($mts_options['mts_pinterest'] == '1') { ?>
										<!-- Pinterest -->
										<span class="share-item pinbtn">
											<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>" class="pin-it-button" count-layout="horizontal">Pin It</a>
										</span>
									<?php } ?>
								</div>
								<!-- end Share Buttons -->
							<?php } ?>
						</div>
						<?php if($mts_options['mts_tags'] == '1') { ?>
							<div class="tags"><?php the_tags('<span class="tagtext">'.__('Tags','mythemeshop').':</span>',', ') ?></div>
						<?php } ?>
						
					</div><!--.post-content box mark-links-->	
					<?php if($mts_options['mts_author_box'] == '1') { ?>
						<div class="postauthor">
							<h4><?php _e('About The Author', 'mythemeshop'); ?></h4>
							<?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '75' );  } ?>
							<h5><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="nofollow"><?php the_author_meta( 'nickname' ); ?></a></h5>
							<p><?php the_author_meta('description') ?></p>
						</div>
					<?php }?> 						
					<?php if($mts_options['mts_related_posts'] == '1') { ?>
						<div class="related-posts">
							<?php
							$categories = get_the_category($post->ID);
							if ($categories) {
							$category_ids = array();
							foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

							$args=array(
							'category__in' => $category_ids,
							'post__not_in' => array($post->ID),
							'posts_per_page'=>4, // Number of related posts that will be shown.
							'ignore_sticky_posts'=>1
							);

							$my_query = new wp_query( $args );
							if( $my_query->have_posts() ) {
							echo '<div class="postauthor-top"><h3>'.__('Related Reviews','mythemeshop').'</h3></div><ul>';
							$counter = '0'; while( $my_query->have_posts() ) { ++$counter; if($counter == 4) { $postclass = 'last'; $counter = 0; } else { $postclass = ''; } $my_query->the_post(); $li = 1; ?>
							<li class="<?php echo $postclass; ?> relatepostli<?php echo $li+$counter; ?>">
								<a rel="nofollow" class="relatedthumb" href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>">
								<span class="rthumb">
									<?php if(has_post_thumbnail()): ?>
										<?php the_post_thumbnail('widgetthumb', 'title='); ?>
										<div class="clear"></div>
										<div class="post-number"><?php comments_number('0','1','%'); ?></div>
									<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/smallthumb.png" alt="<?php the_title(); ?>"  width='80' height='80' class="wp-post-image" />
										<div class="clear"></div>
										<div class="post-number"><?php comments_number('0','1','%'); ?></div>
									<?php endif; ?>
								</span>
								<?php if (strlen($post->post_title) > 52) {
									echo substr(the_title($before = '', $after = '', FALSE), 0, 52) . '...'; } else {
									the_title();
								} ?>

								</a>
							</li>
							<?php } echo '</ul></div>'; }} wp_reset_query(); ?>
						</div><!-- .related-posts -->
					<?php } ?>
					<?php comments_template( '', true ); ?>
				</div><!--.g post-->
			<?php endwhile; /* end loop */ ?>
	</article>
<?php get_sidebar(); ?>
<?php get_footer(); ?>