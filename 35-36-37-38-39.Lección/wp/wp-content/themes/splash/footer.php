<?php $mts_options = get_option('splash'); ?>
	</div><!--#page-->
	<footer>
		<div class="container">
			<div class="footer-widgets">
				<div class="f-widget f-widget-1">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Left Footer') ) : ?><?php endif; ?>
				</div>
				<div class="f-widget f-widget-2">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Center Footer') ) : ?><?php endif; ?>
				</div>
				<div class="f-widget f-widget-3 last">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Right Footer') ) : ?><?php endif; ?>
				</div>
				<div class="copyrights">
					<?php mts_copyrights_credit(); ?>
				</div> 
			</div><!--.footer-widgets-->
		</div><!--.container-->
	</footer><!--footer-->
</div><!--.main-container-->
<?php mts_footer(); ?>
<?php wp_footer(); ?>
<?php if(is_singular()) { ?>
<script type='text/javascript'>
	jQuery(window).load(function() {
	<?php if(is_single()): ?>
	if(jQuery.cookie('rating_<?php echo $post->ID; ?>') == 'true') {
		rating_readOnly = true;
	} else {
		rating_readOnly = false;
	}
	jQuery('.user-rating').raty({
		half: true,
		size: 13,
		path: '<?php bloginfo("template_directory"); ?>/images/stars/',
		starHalf: 'star-half.png',
		starOff: 'star-off.png',
		starOn: 'star-on.png',
		width: 90,
		readOnly: rating_readOnly,
		hintList: ['bad', 'poor', 'okay', 'good', 'excellent'],
		space: false,
		start: <?php echo ts_get_post_score($post->ID); ?>,
		click: function(s, e) {
			jQuery.cookie('rating_<?php echo $post->ID; ?>', true);
			jQuery.ajax({
				url: '<?php echo admin_url("admin-ajax.php"); ?>',
				data: 'action=themesector_rating&rating='+s+'&ip=<?php echo $_SERVER["REMOTE_ADDR"]; ?>&post_id=<?php echo $post->ID; ?>',
				type: 'POST',
				dataType: 'json',
				success: function(data) {
					if(data.success == false) {
						alert('You have already rated this post.');
						jQuery('.user-rating').raty('readOnly', true);
						jQuery('.user-rating').raty('start', <?php echo ts_get_post_score($post->ID); ?>);
					}
				}
			});
		}
	});
	<?php endif; ?>
	});
</script>
<?php } ?>
</body>
</html>