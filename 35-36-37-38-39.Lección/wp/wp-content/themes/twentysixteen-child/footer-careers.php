<!-- /Footer -->
<div class="clearfix"></div>
<!-- Copyright -->
<div id="copyright">
  <div class="inner">
    <div class="row-fluid"> 
      <!-- Copyright Text -->
      <div id="copyright-text">Copyright 2013 <a href="http://uouapps.com/careers">Career</a> | All Rights Reserved | Design by <a href="http://uouapps.com/">UOU Apps</a></div>
      <!-- /Copyright Text --> 
      <!-- Copyright Social Link -->
      <div id="copyright-link">
        <div class="buttons">
          <ul class="top_soical_icons pull-right	">
            <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
            <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
            <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
            <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
            <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
            <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
          </ul>
        </div>
      </div>
      <!-- /Copyright Social Link --> 
      
      <a class="scrollTop" href="#header" style="display: none;">
      <div id = "up_container"> <span></span> </div>
      </a> </div>
  </div>
</div>
<!-- /Copyright -->

<!-- /Copyright -->
<link href="css/switcher.css" rel="stylesheet" type="text/css" />
<div class="demo_changer">
  <div class="demo-icon"></div>
  <div class="form_holder">
    <div class="line"></div>
    <p>CHOOSE YOUR STYLE</p>
    <div class="predefined_styles"> <a href="javascript: void(0)" title="switch styling" id="color_scheme_1" class="switch_color"></a> <a href="javascript: void(0)" title="switch styling" id="color_scheme_2" class="switch_color"></a> <a href="javascript: void(0)" title="switch styling" id="color_scheme_3" class="switch_color"></a> <a href="javascript: void(0)" title="switch styling" id="color_scheme_4" class="switch_color"></a> <a href="javascript: void(0)" title="switch styling" id="color_scheme_5" class="switch_color"></a> <a href="javascript: void(0)" title="switch styling" id="color_scheme_6" class="switch_color"></a> <a href="javascript: void(0)" title="switch styling" id="color_scheme_7" class="switch_color"></a> <a href="javascript: void(0)" title="switch styling" id="color_scheme_8" class="switch_color"></a> </div>
    <div class="line"></div>
    <p>CHOOSE YOUR LAYOUT</p>
    <div class="predefined_styles_layout"> <a href="javascript: void(0)" title="switch styling" id="full-width" class="layoutbtn">Fluid</a> <a href="javascript: void(0)" title="switch styling" id="boxed" class="layoutbtn">Fixed</a> </div>
    <div class="line"></div>
    <p>CHOOSE BACKGROUND</p>
    <div class="predefined_styles_bg"> <a href="javascript: void(0)" title="switch styling" id="pattern_1" class="styling_pattern"></a> <a href="javascript: void(0)" title="switch styling" id="pattern_2" class="styling_pattern"></a> <a href="javascript: void(0)" title="switch styling" id="pattern_3" class="styling_pattern"></a> </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/switcher.js"></script>

</body>
</html>