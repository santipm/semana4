<?php
/**
 * Template Name: Curso de Wordpress
 * Description: Página para pruebas
*/

require('wp-admin/includes/user.php');

if(!current_user_can( 'administrator' )){
	echo "Está el acceso bloqueado";
	exit();}
get_header(); 

if(isset($_GET['accion']) == 'borrar'){
	$id = $_GET['id'];
	echo "Entra";
	if(wp_delete_user( $id)){
		echo "El usuario se ha borrado correctamente";
	}//else{
	//	echo "El usuario NO se ha borrado correctamente";
	//}

}

$arg = array('role'  => 'subscriber',
							'order' => 'DESC');
$users = get_users( $arg );

//print_r($users);
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<div class="panel panel-default">  
		<div class="panel-heading">Panel heading</div>  
			<table class="table"> 
				<thead> 
					<tr> 
						<th>ID</th> 
						<th>Nombre</th> 
						<th>email</th> 
						<th>Acciones</th>
					</tr> 
				</thead> 
				<tbody> 
				<?php  
					foreach ($users as $user):
						echo '<tr>';
						echo "<th scope='row'>$user->ID</th>";
						echo "<td>$user->user_nicename</td>";
						echo "<td>$user->user_email</td>";
						echo "<td><a href='?id=".$user->ID."&accion=borrar'>Borrar</a> | <a href='/wp/editar?id=".$user->ID."'>Editar</a> </td>"; 
						echo '</tr>';
					endforeach; 
				?>
				</tbody> 
			</table> 
	</div>

	</main><!-- .site-main -->

	<?php //get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
