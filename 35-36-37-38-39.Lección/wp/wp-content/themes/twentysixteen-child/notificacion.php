<?php
/**
 * Template Name: Informar de notificación
 * Description: Página para pruebas
*/


if(!current_user_can( 'subscriber' )){
	echo "Está el acceso bloqueado";
	//exit();
}

if($_SERVER['REQUEST_METHOD'] == 'POST'):

	//Recogemos variables
	$verificacion = $_POST['verificacion'];
	$texto 				= $_POST['texto'];
	$userID				= get_current_user_id( );

	//Comprobamos seguridad
	if(wp_verify_nonce( $verificacion , 'mi-accion' )):
		//Insertamos en la base de datos
		$wpdb->insert('wp_notificaciones',
										array(
											"id_user" => $userID,
											"texto"		=> $texto
										),
										array(
											"%d",
											"%s"
										)
									);
		$mensaje = "Formulario Enviado";
	else:
		$mensaje = "No se ha verificado la seguridad del formulario";
	endif;
	//Recoger variables 

endif;



get_header(); 
$wpdb->show_errors();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php 
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			the_content();

			// End of the loop.
		endwhile;
		?>

		<?php if(isset($mensaje)) {echo "<p>$mensaje</p>";} ?>

		<form action="?page_id=<?php echo get_the_ID(); ?>" method="POST">
			<?php wp_nonce_field( 'mi-accion','verificacion' ); ?>
			<label>
				Descripción
			</label>  
			<textarea col="15" rows="5" name="texto"></textarea>
			<p></p>
			<input type="submit" value="Enviar">
		</form>

	</main><!-- .site-main -->

	<?php //get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
