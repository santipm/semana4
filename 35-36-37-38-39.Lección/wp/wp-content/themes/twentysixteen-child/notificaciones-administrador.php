<?php
/**
 * Template Name: Mis notificaciones(Admin)
 * Description: Página para pruebas
*/


if(!current_user_can( 'administrator' )){
	echo "Está el acceso bloqueado";
	exit();
}
		//Insertamos en la base de datos
$notificaciones =	$wpdb->get_results(
										$wpdb->prepare
											("SELECT * FROM wp_notificaciones 
																 WHERE leido = %d",0
											)
									);		


$wpdb->update( 
	'wp_notificacione', 
	array( 
		'leido' => 1,	// string

	), 
	array( 'ID' => 1 ))
 
get_header(); 
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php
			echo '<table>';
			foreach ($notificaciones as $notificacion):
				$usuario = get_user_by('ID', $notificacion->id_user);
				echo "<tr>";
				echo 	"<td>$usuario->user_email</td>";
				echo  "<td>$notificacion->texto</td>";
				echo "</tr>";
			endforeach;
			echo "</table>";
		?>

	</main><!-- .site-main -->

	<?php //get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
