<?php
/**
 * Template Name: Editar perfil
 * Description: Página para pruebas
*/

require('wp-admin/includes/user.php');

if(!current_user_can( 'administrator' )){
	echo "Está el acceso bloqueado";
	exit();}
get_header(); 

if(isset($_GET['id'])){

		$id = $_GET['id'];
		$user = get_userdata($id );

}elseif(isset($_POST['id'])){
	
		$id = (int)$_POST['id'];
		$email = $_POST['email'];
		wp_update_user( array( 'ID' => $id, 'user_email' => $email ) );
		$user = get_userdata($id);
		
}
	
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<form action="http://localhost:8888/wp/editar" method="POST">
			<input type="hidden" name="id" value="<?php echo $user->ID; ?>">
			<input type="text" name="email" value="<?php echo $user->user_email;?>">
			<input type="submit" value="Actualizar">
		</form>

	</main><!-- .site-main -->

	<?php //get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
