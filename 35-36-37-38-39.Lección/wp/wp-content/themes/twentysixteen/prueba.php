<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
		 
		 error_log($paged);
		 
		 $arg = array(
							'posts_per_page' => 1,
							"order" => "ASC",
							"paged" => $paged);?>

		<?php 
				$miBucle = new WP_query($arg); 
				// Pagination fix
$temp_query = $wp_query;
$wp_query   = NULL;
$wp_query   = $miBucle;

		?>
		<?php if ( $miBucle->have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>
	
			<?php


			// Start the loop.
			while ( $miBucle->have_posts() ) : $miBucle->the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			// End the loop.
			endwhile;

			wp_reset_postdata();
			previous_posts_link('Previous');
			next_posts_link('Next');
			$wp_query = NULL;
$wp_query = $temp_query;
		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
