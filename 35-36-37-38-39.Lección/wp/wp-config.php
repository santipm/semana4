<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wp');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'santiago');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'a13santi');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'ui78v4^tP&ecDBKT>[i=Z[VzD~+k^@,8s(K+];Y|O9`:wJ00pD=/wJFy$kj1MfB&');
define('SECURE_AUTH_KEY', '$B5)HjaftWL Xge;`/yQ]n!-O*)3{LN-D$tU`Wo#m3~^*AfPOI6$+s=5Fc_q7I-T');
define('LOGGED_IN_KEY', 'UJbO`VCe1$giv|Kh#4mg,3]O^AvsZ8_Rt 9@<.NOx=57;uyn-ph1WZX;|^|qZ&dF');
define('NONCE_KEY', 'bky9;)$BjdX=]<|u)lvykoy%v<b;%[Iq9@vrt.:YfPqI_;vMb95vi!I,s[wbPmQ1');
define('AUTH_SALT', 'LP78;Q7WBu!`_sCKgIUB(q_bYS^oNl:4E(O|BH46oz)[jIjjBt#8Y&H{S,-b5m~@');
define('SECURE_AUTH_SALT', 'P/kn_.XjPE/S>r@ccxL<ao0Dyio|t9|w*{Y5R30|GP*!>$g8Q&iJL!_o&Ot$6L$x');
define('LOGGED_IN_SALT', 'R7X~NKC-w4pvx++e7k_|4JVQ<LcJf9f]]<NR|I&_]p{SLmzi>Y* Ozz$2/CtY2[g');
define('NONCE_SALT', 'tZ,kB$a![1kpz$?&`(+_knv6xyjUTz<z5].ztNd3i_F[RV:& Zy-#K+y|u![RdB7');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG',true);
define('WP_DEBUG_DISPLAY',false)

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

